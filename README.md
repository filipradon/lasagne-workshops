# Lasagne's workshop README

## Preparation

IDE: Pycharm is recommended, however Sublime Text, vim etc will work

Download Anaconda Python (2.7) from here:

`https://www.continuum.io/downloads`

and follow instruction on the site.

Create Anaconda env as below:

`conda create --name lasagne`

After installation is completed you need to activate env with:

`source activate lasagne`

Env can be deactivated with:

`source deactivate`

All installations listed below shall be done within activated env.

Then we need to install couple of additional dependencies:

`pip install docopt`

`pip install tqdm`

`conda install opencv`

`conda install theano pygpu`

Then clone fresh version from Lasagne github:

`git clone https://github.com/Lasagne/Lasagne.git`

And install it with:

`python setup.py install`

At last, we need to install jupyter notebooks requirements:

`pip install -r notebooks/requirements.txt`

## Theano jupyter notebook

Before first run:

`jupyter nbextension enable --py widgetsnbextension`

Run notebook from commandline with:

`jupyter notebook`

You can execute cells with shift+enter shortcut.

## Exercises

From root of repository:

`cp .env.example .env`

Then:

`vim .env`

And set DATA_ROOT environmental variable to some directory where you store all project related data.

You can export this as below:

`source export_envs.sh`

Download pretrained models into your data directory:

`wget https://s3.amazonaws.com/lasagne/recipes/pretrained/imagenet/vgg19_normalized.pkl`

`wget https://s3.amazonaws.com/lasagne/recipes/pretrained/imagenet/vgg16.pkl`

### Exercise 1: Run simple mnist example

Checkout to branch simple-mnist:

`git checkout simple-mnist`
 

You can run dense mnist network with as follows:

`python exercises/mnist_dense.py`

To show usage of script, type:

`python exercises/mnist_dense.py --help`

After default run, you'll get learning curve plot in your exercise directory.

Now it's the time to manipulate models and trainers.

Implement your own architecture in `models/mnist_dense.py` similarly to SmallDense model.

Then you can import it and use it in `exercises/mnist_dense.py`.
 
When you look into `training.trainers` there are 4 different optimizers:
* sgd

* momentum sgd

* nesterovd sgd

* adam

Check out how they impact training speed and accuracy.

### Exercise 2: Run convolutional mnist example

First:

`git checkout conv-mnist`

In this exercise there's some coding required. Please follow TODOs in `exercises/mnist_conv.py`

When you manage to train convnet, we encourage you to try your own architecture and manipulate with different optimizers.

### Exercise 3: Transfer learning

First:

`git checkout flowers-step-1`

We need to implement:

* data loader in `loaders.flowers.py`

* constructor of FlowersVGG16 model

Then we can run `python exercises/flowers_conv.py`. If it succeeded then process, if not you can go to branch 
flowers-step-2, where it's up and running.

Now, we will train model with and without pretrained network to observe how quickly it converges and if it overfits to data.

To speed up training (usually slightly worse accuracy) we can finetune only last couple layers of VGG16 network.

All trainers posses kwarg called n_last_layers to specify this.

### Exercise 4: Art style transfer



