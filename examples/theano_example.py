import theano.tensor as T
from theano import function

# declaring variables
x = T.dscalar('x')
y = T.dscalar('y')

# building an expression using the defined variables
z = x + y

# compiling the expression
f = function([x, y], z)

# executing the compiled expression
print f(2, 3)
# >>> array(5.0)
