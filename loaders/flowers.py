import os
import os.path as op

import cv2
import numpy as np

from loaders import Dataset
from utils.file_system import read_json


class Flowers(Dataset):
    def __init__(self, dataset_name, img_resolution=(224, 224), data_root=None):
        self.img_resolution = img_resolution
        self.data_root = data_root or os.getenv("DATA_ROOT")
        flowers_root_dir = op.join(self.data_root, "flower_photos")
        split_path = op.join(flowers_root_dir, "flower_photos_split.json")
        split_dictionary = read_json(split_path)

        dataset_dictionary = split_dictionary[dataset_name]

        classes_names = sorted(set(dataset_dictionary.values()))
        self.labels_mapping = {class_name: index for index, class_name in enumerate(classes_names)}

        print("Flowers dataset: {ds_name} size: {size}".format(ds_name=dataset_name,
                                                               size=len(dataset_dictionary)))

        self.images, self.targets = [], []

        for img_sub_path, img_label in dataset_dictionary.iteritems():
            img_path = op.join(flowers_root_dir, img_sub_path)
            img = self.load_img(img_path)
            label_id = self.labels_mapping[img_label]
            self.images.append(img)
            self.targets.append(label_id)
        print("Flowers dataset: images from {ds_name} loaded".format(ds_name=dataset_name))

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        return self.images[index], self.targets[index]

    def load_img(self, img_path):
        img = cv2.imread(img_path)
        resized_img = cv2.resize(img, tuple(list(self.img_resolution)[::-1]))
        return resized_img.astype(np.float32)
