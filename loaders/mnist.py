from loaders import Dataset
from utils.file_system import unpickle
import os
import os.path as op


class MNIST(Dataset):
    DATASET_NAME_TO_ID = {
        "train": 0,
        "valid": 1,
        "test": 2
    }

    def __init__(self, dataset_name, data_root=None):
        self.data_root = data_root or os.getenv("DATA_ROOT")
        mnist_path = op.join(self.data_root, "mnist", "mnist.pkl")
        mnist_data = unpickle(mnist_path)
        try:
            self.images = mnist_data[self.DATASET_NAME_TO_ID[dataset_name]][0]
            self.targets = mnist_data[self.DATASET_NAME_TO_ID[dataset_name]][1]
        except KeyError:
            raise ValueError("Supported dataset names: {'train', 'valid', 'test'}")

    def __len__(self):
        return self.images.shape[0]

    def __getitem__(self, index):
        return self.images[index], self.targets[index]
