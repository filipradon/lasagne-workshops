from abc import ABCMeta, abstractmethod

import numpy as np
import theano
import theano.tensor as T
from lasagne.objectives import categorical_accuracy, aggregate, categorical_crossentropy
from lasagne.updates import sgd, momentum, nesterov_momentum, adam
from tqdm import tqdm

from loaders import DataLoader


class BaseTrainer(object):
    __class__ = ABCMeta
    BAR_FORMAT = "{percentage:3.0f}%|{bar}| {n_fmt}/{total_fmt} [{elapsed}<{remaining}, {desc}]"

    def __init__(self, model, epochs, batch_size, learning_rate, input_type="dense", transformer=lambda x: x,
                 learning_rate_strategy=lambda lr, epoch: lr, last_n_layers=None):
        self.last_n_layers = last_n_layers
        self.input_type = input_type
        self.learning_rate_strategy = learning_rate_strategy
        self.tf_train = None
        self.tf_test = None
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.epochs = epochs
        self.model = model
        self.transformer = transformer
        print("Compiling theano model ...")
        self.compile()

    def compile(self):
        if self.input_type == "dense":
            t_images = T.fmatrix('images')
        elif self.input_type == "conv":
            t_images = T.ftensor4('images')
        else:
            raise NotImplementedError("Not implemented input type. Currently implemented are {'dense', 'conv'}")

        t_targets = T.ivector('targets')

        t_learning_rate = T.fscalar('learning_rate')

        train_predictions = self.model.get_output(t_images)
        test_predictions = self.model.get_output(t_images, deterministic=True)

        train_loss = self.loss(train_predictions, t_targets)
        train_accuracy = self.accuracy(train_predictions, t_targets)

        test_loss = self.loss(test_predictions, t_targets)
        test_accuracy = self.accuracy(test_predictions, t_targets)

        params = self.model.get_parameters(trainable=True)
        if self.last_n_layers is not None and type(self.last_n_layers) is int:
            params = params[-self.last_n_layers:]
        updates = self.build_updates(train_loss, params, t_learning_rate)

        self.tf_train = theano.function(inputs=[t_images, t_targets, t_learning_rate],
                                        outputs=[train_loss, train_accuracy], updates=updates)

        self.tf_test = theano.function(inputs=[t_images, t_targets], outputs=[test_loss, test_accuracy])

    @abstractmethod
    def build_updates(self, loss, params, learning_rate):
        pass

    def train(self, train_dataset, validation_dataset):
        train_loader = DataLoader(train_dataset, self.batch_size, shuffle=True, transformer=self.transformer)
        validation_loader = DataLoader(validation_dataset, self.batch_size, shuffle=False, transformer=self.transformer)

        train_accuracies, validation_accuracies = [], []
        train_losses, validation_losses = [], []

        for epoch_id in xrange(self.epochs):
            print "Epoch: {epoch}".format(epoch=(epoch_id + 1))

            train_loss, train_accuracy = self.__train(train_loader, epoch_id + 1)

            train_accuracies.append(train_accuracy)
            train_losses.append(train_loss)

            validation_loss, validation_accuracy = self.__test(validation_loader, val=True)

            validation_accuracies.append(validation_accuracy)
            validation_losses.append(validation_loss)

        return np.array(train_accuracies), np.array(train_losses), np.array(validation_accuracies), \
               np.array(validation_losses)

    def __train(self, train_loader, epoch):
        train_accuracy = 0.
        train_loss = 0.
        count = 0
        self.learning_rate = self.learning_rate_strategy(self.learning_rate, epoch)
        train_iterator = tqdm(train_loader.generate_mini_batches(), total=len(train_loader),
                              bar_format=self.BAR_FORMAT)

        for images, targets in train_iterator:
            batch_loss, batch_accuracy = self.tf_train(images, targets, self.learning_rate)
            count += 1
            batch_ratio = 1. / count
            epoch_ratio = (count - 1.) / count

            train_accuracy = train_accuracy * epoch_ratio + batch_accuracy * batch_ratio
            train_loss = train_loss * epoch_ratio + batch_loss * batch_ratio

            train_iterator.desc = "Train accuracy: {accuracy:.2f}, Train loss: {loss:.2f}".format(
                accuracy=train_accuracy, loss=train_loss)
        return train_loss, train_accuracy

    def __test(self, test_loader, val=False):
        set_name = "Val" if val else "Test"
        test_iterator = tqdm(test_loader.generate_mini_batches(), total=len(test_loader),
                             bar_format=self.BAR_FORMAT)
        test_accuracy = 0
        test_loss = 0
        count = 0

        for images, targets in test_iterator:
            batch_loss, batch_accuracy = self.tf_test(images, targets)
            count += 1
            batch_ratio = 1. / count
            epoch_ratio = (count - 1.) / count

            test_accuracy = test_accuracy * epoch_ratio + batch_accuracy * batch_ratio
            test_loss = test_loss * epoch_ratio + batch_loss * batch_ratio
            test_iterator.desc = "{set_name} accuracy: {accuracy:.2f}, {set_name} loss: {loss:.2f}".format(
                accuracy=test_accuracy, loss=test_loss, set_name=set_name)
        return test_loss, test_accuracy

    def test(self, dataset):
        test_loader = DataLoader(dataset, self.batch_size, shuffle=False, transformer=self.transformer)
        return self.__test(test_loader)

    @staticmethod
    def accuracy(predictions, targets):
        return aggregate(categorical_accuracy(predictions, targets))

    @staticmethod
    def loss(predictions, targets):
        return aggregate(categorical_crossentropy(predictions, targets))


class SGDTrainer(BaseTrainer):
    def build_updates(self, loss, params, learning_rate):
        return sgd(loss, params, learning_rate)


class MomentumTrainer(BaseTrainer):
    def __init__(self, model, epochs, batch_size, learning_rate, momentum_factor, input_type="dense",
                 transformer=lambda x: x, learning_rate_strategy=lambda lr, epoch: lr, last_n_layers=None):
        self.momentum_factor = momentum_factor
        super(MomentumTrainer, self).__init__(model, epochs, batch_size, learning_rate, input_type=input_type,
                                              transformer=transformer, learning_rate_strategy=learning_rate_strategy,
                                              last_n_layers=last_n_layers)

    def build_updates(self, loss, params, learning_rate):
        return momentum(loss, params, learning_rate, momentum=self.momentum_factor)


class NesterovTrainer(MomentumTrainer):
    def build_updates(self, loss, params, learning_rate):
        return nesterov_momentum(loss, params, learning_rate, momentum=self.momentum_factor)


class AdamTrainer(BaseTrainer):
    def build_updates(self, loss, params, learning_rate):
        return adam(loss, params, learning_rate)
