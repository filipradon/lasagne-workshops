import cPickle
import json
import os
import os.path as op


def read_json(path):
    path = op.expanduser(path)
    with open(path, 'r') as f:
        return json.load(f)


def write_json(obj, path):
    path = op.expanduser(path)
    with open(path, 'w') as f:
        json.dump(obj, f, indent=4)


def unpickle(path):
    path = op.expanduser(path)
    with open(path, 'r') as f:
        return cPickle.load(f)


def pickle(obj, path):
    path = op.expanduser(path)
    with open(path, 'w') as f:
        cPickle.dump(obj, f, protocol=cPickle.HIGHEST_PROTOCOL)


def create_dir_if_does_not_exist(path):
    path = op.expanduser(path)
    if not op.exists(path):
        os.makedirs(path)


def list_dirs(root_dir):
    return [d for d in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir, d))]


def list_images_from_dir(dir_name):
    return [p for p in os.listdir(dir_name) if p.endswith(('jpg', 'jpeg', 'png', 'JPG'))]
